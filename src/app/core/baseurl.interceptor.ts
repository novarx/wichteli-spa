import { Injectable } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class BaseurlInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        if (!request.url.match(/^https?:\/\//)) {
            const newRequest = request.clone({url: this.getUrl(request.url)});
            request = Object.assign(request, newRequest);
        }
        return next.handle(request);
    }

    private getUrl(path: string) {
        path = !path.startsWith('/') ? '/' + path : path;
        return environment.baseUrl + path;
    }
}
