import { fakeAsync, tick } from '@angular/core/testing';

import { LoginComponent } from './login.component';
import { ComponentTest } from '../../_testing/component-test';
import { AuthService } from '../auth.service';
import { Observable, of, throwError } from 'rxjs';
import { Router } from '@angular/router';

describe('LoginComponent', () => {
    const router: Partial<Router> = {
        navigateByUrl(commands, extras?) {
            return null;
        }
    };

    const authService: Partial<AuthService> = {
        login(username: string, password: string): Observable<boolean> {
            return of();
        }
    };
    const c: ComponentTest<LoginComponent> = new ComponentTest({
        component: LoginComponent,
        providers: [
            {provide: AuthService, useValue: authService},
            {provide: Router, useValue: router}
        ]
    });
    c.setup();

    it('should create', () => {
        const matContent = 'mat-card mat-card-content form[fxLayout="column"]';
        expect(c.component).toBeTruthy();
        expect(c.elementsTexts('mat-card mat-card-title')).toContain('Login');

        expect(c.countElements(matContent + ' mat-label')).toEqual(2);
        expect(c.elementsTexts(matContent + ' mat-label')).toContain('Email');
        expect(c.elementsTexts(matContent + ' mat-label')).toContain('Passwort');
        expect(c.countElements(matContent + ' mat-form-field input[type="password"][name="password"]')).toEqual(1);
        expect(c.countElements(matContent + ' mat-form-field input[type="text"][name="email"]')).toEqual(1);

        expect(c.elementsTexts(matContent + ' div[fxFlexAlign="center"] button[mat-raised-button]')).toContain('Login');
    });

    it('should set values in model', fakeAsync(() => {
        c.setValueOf(c.element('input[name="email"]'), 'aEmailAddress');
        expect(c.component.form.controls.email.value).toEqual('aEmailAddress');

        c.setValueOf(c.element('input[name="password"]'), 'aPassword');
        expect(c.component.form.controls.password.value).toEqual('aPassword');
    }));

    it('should call login on button click', fakeAsync(() => {
        spyOn(authService, 'login').and.returnValue(of(true));
        spyOn(router, 'navigateByUrl');

        submitValidForm();

        c.expectErrors(0);
        expect(authService.login).toHaveBeenCalledWith('info@example.org', 'aPassword');
        expect(authService.login).toHaveBeenCalledTimes(1);
        expect(router.navigateByUrl).toHaveBeenCalledWith('/');
    }));

    it('should not call login on button click when not valid', fakeAsync(() => {
        spyOn(authService, 'login');

        c.clickOn(c.element('button'));
        c.component.form.markAllAsTouched();
        c.fixture.detectChanges();

        c.expectErrors(2);
        c.expectErrorsToContain('Bitte eine gültige Email-Adresse eingeben');
        c.expectErrorsToContain('Bitte ein Passwort eingeben');
        expect(authService.login).not.toHaveBeenCalled();
    }));

    it('should show server error', fakeAsync(() => {
        spyOn(authService, 'login').and.returnValue(throwError({message: 'Lorem Ipsum dolor'}));
        submitValidForm();

        c.expectErrors(1);
        c.expectErrorsToContain('Lorem Ipsum dolor');
    }));

    function submitValidForm() {
        c.setValueOf(c.element('input[name="email"]'), 'info@example.org');
        c.setValueOf(c.element('input[name="password"]'), 'aPassword');
        c.clickOn(c.element('button'));
        c.component.form.markAllAsTouched();
        c.fixture.detectChanges();
        tick();
    }
});
