import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

class ServerErrors {
    errors: string[];
}

@Component({
    selector: 'wic-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    form: FormGroup;
    serverError: ServerErrors;

    constructor(private router: Router,
                private authService: AuthService,
                private fb: FormBuilder) {
        this.form = this.fb.group({
            email: ['', [Validators.email, Validators.required]],
            password: ['', [Validators.required]]
        });
    }

    ngOnInit(): void {
    }

    login() {
        if (this.form.valid) {
            this.authService.login(this.form.controls.email.value, this.form.controls.password.value).subscribe(
                () => {
                    this.serverError = null;
                    this.router.navigateByUrl('/');
                },
                (e) => this.serverError = e.message
            );
        }
    }

}
