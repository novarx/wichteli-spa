import { TestBed } from '@angular/core/testing';

import { BaseurlInterceptor } from './baseurl.interceptor';
import { HttpEvent, HttpHandler, HttpRequest } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

describe('BaseurlInterceptor', () => {
    let interceptor: BaseurlInterceptor;
    const next: Partial<HttpHandler> = {
        handle(req: HttpRequest<any>): Observable<HttpEvent<any>> {
            return null;
        }
    };

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                BaseurlInterceptor
            ]
        });
        interceptor = TestBed.inject(BaseurlInterceptor);
    });

    it('should be created', () => {
        expect(interceptor).toBeTruthy();
    });

    it('should add baseUrl', () => {
        spyOn(next, 'handle');
        const request: HttpRequest<unknown> = new HttpRequest<unknown>('GET', '/path');

        interceptor.intercept(request, next as any);

        expect(request.url).toEqual(environment.baseUrl + '/path');
        expect(next.handle).toHaveBeenCalledWith(request);
    });

    it('should add baseUrl when path does not start with /', () => {
        spyOn(next, 'handle');
        const request: HttpRequest<unknown> = new HttpRequest<unknown>('GET', 'path');

        interceptor.intercept(request, next as any);

        expect(request.url).toEqual(environment.baseUrl + '/path');
        expect(next.handle).toHaveBeenCalledWith(request);
    });

    it('should still contain body after convertion', () => {
        spyOn(next, 'handle');
        const body = {anAttribute: 'lorem', anInteger: 55};
        const request: HttpRequest<unknown> = new HttpRequest<unknown>('POST', 'path', body);

        interceptor.intercept(request, next as any);

        expect(request.body).toEqual(body);
    });

    it('should not add baseUrl when starts with http', () => {
        spyOn(next, 'handle');
        const request: HttpRequest<unknown> = new HttpRequest<unknown>('GET', 'https://novarx.ch/');

        interceptor.intercept(request, next as any);

        expect(request.url).toEqual('https://novarx.ch/');
        expect(next.handle).toHaveBeenCalledWith(request);
    });
});
