import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NavigationComponent } from './core/navigation/navigation.component';
import { MaterialModule } from './_modules/material.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { AppRoutingModule } from './_modules/app.routing.module';
import { WelcomeComponent } from './core/welcome/welcome.component';
import { LoginComponent } from './core/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { BaseurlInterceptor } from './core/baseurl.interceptor';

@NgModule({
    declarations: [
        AppComponent,
        NavigationComponent,
        WelcomeComponent,
        LoginComponent,
    ],
    imports: [
        HttpClientModule,
        BrowserModule,
        MaterialModule,
        FlexLayoutModule,
        AppRoutingModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: BaseurlInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
