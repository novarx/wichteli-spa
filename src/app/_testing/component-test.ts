import { ComponentFixture, fakeAsync, TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { Provider, Type } from '@angular/core';
import { MatTabsModule } from '@angular/material/tabs';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatChipsModule } from '@angular/material/chips';
import { MatIconModule } from '@angular/material/icon';
import { RouterTestingModule } from '@angular/router/testing';

export interface ComponentTestConfig<T> {
    component: Type<T>;
    declerations?: any[];
    providers: Provider[];
    imports?: any[];
    beforeCreation?: () => void;
}

export class ComponentTest<T> {
    public component: T;
    public fixture: ComponentFixture<T>;
    public parent: HTMLElement;
    private config: ComponentTestConfig<T>;

    constructor(config: ComponentTestConfig<T>) {
        this.config = config;
        if (this.config.declerations) {
            this.config.declerations = [
                this.config.component,
                ...this.config.declerations
            ];
        } else {
            this.config.declerations = [this.config.component];
        }
    }

    public setup() {
        beforeEach(fakeAsync(() => {
            TestBed.configureTestingModule({
                imports: [
                    HttpClientTestingModule,
                    FormsModule,
                    ReactiveFormsModule,
                    MatFormFieldModule,
                    MatInputModule,
                    NoopAnimationsModule,
                    MatTabsModule,
                    MatCardModule,
                    MatListModule,
                    MatChipsModule,
                    MatIconModule,
                    RouterTestingModule
                ],
                declarations: this.config.declerations,
                providers: this.config.providers
            }).compileComponents();

            if (this.config.beforeCreation) {
                this.config.beforeCreation();
            }
            this.fixture = TestBed.createComponent(this.config.component);
        }));
        beforeEach(() => {
            this.component = this.fixture.componentInstance;
            this.fixture.detectChanges();
            this.parent = this.fixture.nativeElement;
        });
    }

    private errors(): string[] {
        return Array.from(
            this.parent.querySelectorAll('.mat-error')
        ).map(v => v.textContent.trim());
    }

    public countErrors(): number {
        return this.errors().length;
    }

    public expectErrors(count: number): void {
        expect(this.countErrors()).toEqual(count);
    }

    public expectErrorsToContain(message: string): void {
        expect(this.errors()).toContain(message);
    }

    public clickOn(inputElement: HTMLElement): void {
        inputElement.dispatchEvent(new Event('click'));
    }

    public setValueOf(inputElement: HTMLElement | HTMLInputElement, value: any): void {
        (inputElement as HTMLInputElement).value = value;
        inputElement.dispatchEvent(new Event('input'));
    }

    public element(cssQuery: string): HTMLElement {
        return this.parent.querySelector(cssQuery) as HTMLElement;
    }

    public elements(cssQuery: string): HTMLElement[] {
        return Array.from(this.parent.querySelectorAll(cssQuery))
            .map(element => element as HTMLElement);
    }

    public elementsTexts(cssQuery: string): string[] {
        return Array.from(this.parent.querySelectorAll(cssQuery))
            .map(element => element.textContent.trim());
    }

    public countElements(query: string): number {
        return this.parent.querySelectorAll(query).length;
    }

}
