import { matModules } from '../_modules/material.module';
import { RouterTestingModule } from '@angular/router/testing';

export const defaultImports = [...matModules, RouterTestingModule];
