import { async, TestBed } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { NavigationComponent } from './core/navigation/navigation.component';
import { defaultImports } from './_testing/component-testing';

describe('AppComponent', () => {
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [
                AppComponent,
                NavigationComponent
            ],
            imports: [...defaultImports]
        }).compileComponents();
    }));

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
    });

});
