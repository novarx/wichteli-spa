import { Component } from '@angular/core';

@Component({
    selector: 'wic-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    title = 'wichteli-spa';
}
